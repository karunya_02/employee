package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import com.dao.Departmentdao;
import com.model.Department;

@RestController
public class DepartmentController {

	@Autowired
	Departmentdao deptdao;
	
	@GetMapping("getDepartments")
	public List<Department> getDepartments() {
		return deptdao.getDepartments();
	}
	
	@GetMapping("getDepartmentById/{id}")
	public Department getDepartmentById(@PathVariable("id") int deptId) {
		return deptdao.getDepartmentById(deptId);
	}
	
	@GetMapping("getDepartmentByName/{name}")
	public Department getDepartmentByName(@PathVariable("name") String deptName) {
		return deptdao.getDepartmentByName(deptName);
	}
	
}
